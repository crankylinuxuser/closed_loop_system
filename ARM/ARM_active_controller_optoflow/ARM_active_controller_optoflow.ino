volatile long requestedStepInput = 0;     // This is the position in steps the RAMPS requests
                                          // We will acquire this from external interrupt 0. Note it can go negative.
volatile bool requestedMotorDirection = 0 ; // 1 is "forward" steps, and 0 is reverse steps
volatile bool requestedMotorON = 0 ;

volatile long sensorStepInput = 0;



// PIN DEFINITIONS for pololu/A4988 passthrough
const byte stepIN = 25 ;        // Think of this as the steps coming in FROM ramps
                                // pin D2 arduino name, interrupt pin 0
const byte dirIN = 26 ;         // pin D3 arduino name, interrupt pin 1
const byte motorEnableIN = 27 ; // This is the pin for watching Motor enable
                                // This var is inverse of what you expect. 0 is ON, 1 is OFF. Read A4988 chip spec
const byte stepOUT = 28;        // Pin that goes to the stepper driver pin
const byte dirOUT = 29;         // Pin that goes to the direction pin
const byte motorEnableOUT = 30; // Pin that connects to ENABLE pin

char incomingByte;              // a variable to read incoming serial data into
String incomingCommand;         // Varible to store the command

HardwareTimer timer(1);         // 
#define MOTOR_SPEED 1000        // 1000 us = 10 KHz , this will NEED changed to faster
// --------------------------------------------------------------------------------------------------


// INTERRUPT #0, steps coming IN. A step represents only 1 step. These can come in FAST, but Mega+RAMPS goes at 10kHz
// A4988 specifies that steps can come in as fast as 1uS (1MHz). If that was the case, we'd be using a Cortex M3 or 4 for timing
void stepTrigger()
{
  if (requestedMotorDirection == 1) { requestedStepInput++ ; }          // if requestedMotorDirection is 1, add step to requestedStepInput
  else { requestedStepInput-- ; }    // if requestedMotorDirection is 0, subtract step from requestedStepInput
}

// INTERRUPT #1, direction of motor
void directionTrigger()
{
  requestedMotorDirection = digitalRead(dirIN) ; // We cant be sure of initial state, so we always just set it from a read
  digitalWrite(dirOUT, requestedMotorDirection) ;
}

// INTERRUPT #2, direction of motor
void motorTrigger()
{
  requestedMotorON = digitalRead(motorEnableIN) ;
  digitalWrite(motorEnableOUT, requestedMotorON) ;
}

// TIMER INTERRUPT #3, moves motor at 10KHz pulses
void moveMotor()
{
 // Code being tested before released 
}

// --------------------------------------------------------------------------------------------------


void setup() {
  // put your setup code here, to run once:
  pinMode(stepIN, INPUT);
  pinMode(dirIN, INPUT);
  pinMode(motorEnableIN, INPUT);
  pinMode(stepOUT, OUTPUT);
  pinMode(dirOUT, OUTPUT);
  pinMode(motorEnableOUT, OUTPUT);

  digitalWrite(stepOUT, LOW);
  digitalWrite(dirOUT, LOW);
  digitalWrite(motorEnableOUT, digitalRead(motorEnableIN) );

  attachInterrupt(stepIN, stepTrigger, RISING);
  attachInterrupt(dirIN, directionTrigger, CHANGE);
  attachInterrupt(motorEnableIN, motorTrigger, CHANGE);

  timer.pause();
  timer.setPeriod(MOTOR_SPEED); // in microseconds
  timer.setChannel1Mode(TIMER_OUTPUT_COMPARE);
  timer.setCompare(TIMER_CH1, 1);  // Interrupt 1 count after each update
  timer.attachCompare1Interrupt(moveMotor);
  timer.refresh();
  timer.resume();

  Serial.begin(115200);
}

void loop() {

    if (Serial.available() > 0) {
      incomingByte = Serial.read();
      incomingCommand.concat(String(incomingByte));
    }
    if ( incomingCommand.endsWith(".") || incomingCommand.endsWith("?") ){
      executeCode(incomingCommand);
      incomingCommand.remove(0);
    }
    if ( incomingCommand.length() > 11 ){
      Serial.println("Length of command exceeded.");
      incomingCommand.remove(0);
    }

}

// --------------------------------------------------------------------------------------------------



void executeCode(String code) {
  long int value ;
  int spaceIndex = code.indexOf(' '); // Figure out where the space is in the command, else -1
  String command;
  
  if (spaceIndex == -1){
    value = -1;
    if(code.endsWith(".")){ 
      command = code.substring(0,code.indexOf('.'));
    }
    else{
      command = code.substring(0,code.indexOf("?"));
    }
  } 
  else {
    value = code.substring((spaceIndex+1), code.length()).toInt();
    command = code.substring(0,spaceIndex);
  }
    
    // if(command == "NAME OF COMMAND") {
    // Do stuff ; Do More stuff ;
    // }
      
    if(command == "POS"){          // prints position
      Serial.print("Steps from initial: ") ; Serial.println(requestedStepInput);
    }
    if(command == "DIR"){          // prints position
      Serial.print("Motor direction is: ") ; Serial.println(requestedMotorDirection);
    }
    if(command == "ZERO"){
      requestedStepInput = 0 ;
      Serial.println("Steps is set to Zero.");
    }
    if(command == "ENABLE"){
      Serial.print("Motor enable status: ") ; Serial.println(!requestedMotorON);
    }
    
    return ;
}



