// User changable variables
String serialName = "/dev/ttyACM1" ; // Ugly, but Linux has loads of serial ports. Change it to yours.
int baudRate = 115200 ; // Set your baud rate here. 
int X_val = 30; // VALUE from ADNS3080 sensor. 
int Y_val = 30; //VALUE from ADNS3080 sensor
int multiplier = 16; // Size to multiply to get a prettier webcam pic

// Serial handling declarations
import processing.serial.*;
Serial myPort;  // Create object from Serial class
int val;      // Data received from the serial port

// The picture 2d array. Global scope makes me sad.
int[][] picture;



void setup() 
{
  // Create the 2d array, and init it to 0. From 0's, it's only changed by convImage(), but is always 'data', never null.
  picture = new int[X_val][Y_val];
  for(int i = 0; i < Y_val; i++){
    for(int j = 0; j < X_val; j++){
      picture[i][j] = 0;
    }
  }
  
  // We want the window size to be X and Y, scaled by the multiplier
  size((X_val*multiplier), (Y_val*multiplier));
  
  // Serial declaration
  myPort = new Serial(this, serialName, baudRate);
  
  noStroke(); // Cause we don't want lines between the pixels of the camera!
}


void draw() {
  String myString = null; // We zero out the incoming serial string every time we go through the loop
  // If we get data over the serial port, terminate the string on "*".
  if (myPort.available() > 0) {
    myString = myPort.readStringUntil(int('*')); 
  }
  
  // If we have data in myString, process it. We dont want this in our serial loop. If we get bad data, we get a null pointer exeption :(
  if (myString != null){
    convImage(myString); // Crap hack that copies in place this data to picture[][]. Would be beautiful IF fcn's could return arrays
  } 
  
  // Side effects by way of drawing stuff on screen. Nature of this language.
  drawCam(picture);
}


// This fcn takes in a complete string from the micro that entails a "frame" from the sensor. The format of this data is simple - 
// It's the sequential list of grayscale values seperated by spaces, with a "*\r" at the end. 
// It looks like:  12 32 4 0 0 16 24 54 38 ...(snipped)... 32 42 *\r
// The format represents a standard webcam format where upper left is 0,0 , and going right is +X and going down is +Y.
// 1 2 3 4
// 5 6 7 8 
// 9 10 11 12
void convImage(String imageString){
  String[] imagearray = splitTokens(imageString); // Split the spaces into a big 1d array
  for (int a = 0; a < (imagearray.length-1); a++){ // To count the imagearray index
    picture[a/Y_val][a%X_val] = int(imagearray[a]); // This shoves the 1d array data into appropriate 2d in picture[][]
  }
}  


// This simply draws the window's contents - the actual webcam picture. 
// Warning, depending on sensor, you might have to good around with rotating or flipping the picture.
// Dont trust StackExchange. They have horrible stupid O(N^2) algos there. It should be a constant time convert or read in-place
void drawCam(int[][] pics){
  for(int i = 0; i < X_val; i++){
    for(int j = 0; j < Y_val; j++){
      fill(pics[(X_val-1)-i][(Y_val-1)-j]);   // Puts in value for grayscale color
      // fill(pics[i][j]); // is updside down for me. turning cam fixes view. So does counting down as above
      // Rule of thumb - i,j at 0,0 incrementing positive is default. My sensor requires decrementing from max i,j to flip correctly. 
      // Try different combinations of [i] [j] [29-i] [29-j] for relative indexes until you find yours.
      rect((i*multiplier), (j*multiplier), multiplier, multiplier);
    }
  }
}
