This will be the location for the board designs. 

We have 2 board designs:

Fritzing : Used to make pretty pictures. Useless otherwise.

KiCAD : Library parts are DONE
        Schematic is DONE.
        PCB Footprints are DONE
        PCB is DONE
        Gerbers are DONE

Boards are ordered via Seeed Studio.