volatile long stepInput = 0;     // This is the position in steps the stepper motor has done.
// We will acquire this from external interrupt 0. Note it can go negative.
volatile bool motorDirection = 0 ; // 1 is "forward" steps, and 0 is reverse steps
volatile bool motorON = 0 ;

// PIN DEFINITIONS for pololu/A4988 passthrough
const byte stepIN = 2 ;     // Think of this as the steps coming in FROM ramps
// pin D2 arduino name, interrupt pin 0
const byte dirIN = 3 ;      // pin D3 arduino name, interrupt pin 1
const byte motorEnableIN = 4 ; // This is the pin for watching Motor enable
// This var is inverse of what you expect. 0 is ON, 1 is OFF. Read A4988 chip spec
const byte stepOUT = 5; // Pin that goes to the stepper driver pin
const byte dirOUT = 6; // Pin that goes to the direction pin
const byte motorEnableOUT = 7; // Pin that connects to ENABLE pin

char incomingByte;                    // a variable to read incoming serial data into
String incomingCommand; // Varible to store the command


// --------------------------------------------------------------------------------------------------


// INTERRUPT #0, steps coming IN. A step represents only 1 step. These can come in FAST, but Mega+RAMPS goes at 10kHz
// A4988 specifies that steps can come in as fast as 1uS (1MHz). If that was the case, we'd be using a Cortex M3 or 4 for timing
void stepTrigger()
{
  if (motorDirection == 1) { stepInput++ ; }          // if motorDirection is 1, add step to stepInput
  else { stepInput-- ; }    // if motorDirection is 0, subtract step from stepInput
  digitalWrite(stepOUT, 1); // Rapidly toggle the stepOUT pin reasonably fast - possibly will need PORTD method
  digitalWrite(stepOUT, 0);
}

// INTERRUPT #1, direction of motor
void directionTrigger()
{
  motorDirection = digitalRead(dirIN) ; // We cant be sure of initial state, so we always just set it from a read
  digitalWrite(dirOUT, motorDirection);
}

// --------------------------------------------------------------------------------------------------


void setup() {

  // Pins coming in from RAMPS
  pinMode(stepIN, INPUT);
  attachInterrupt(digitalPinToInterrupt(stepIN), stepTrigger, RISING);
  pinMode(dirIN, INPUT);
  attachInterrupt(digitalPinToInterrupt(dirIN), directionTrigger, CHANGE);
  pinMode(motorEnableIN, INPUT);

  // Pins going OUT to Pololu/A4988 board
  pinMode(stepOUT, OUTPUT);
  pinMode(dirOUT, OUTPUT);
  pinMode(motorEnableOUT, OUTPUT);
  motorON = digitalRead(motorEnableIN);
  digitalWrite(motorEnableOUT, motorON ) ;
  Serial.begin(115200);
}

// --------------------------------------------------------------------------------------------------


void loop() {
  if (motorON != digitalRead(motorEnableIN)) {
    motorON = !motorON;
    digitalWrite(motorEnableOUT, motorON ) ;
    Serial.print("Motor enable status: ") ; Serial.println(!motorON);
  }

    if (Serial.available() > 0) {
      incomingByte = Serial.read();
      incomingCommand.concat(String(incomingByte));
    }
    if ( incomingCommand.endsWith(".") || incomingCommand.endsWith("?") ){
      executeCode(incomingCommand);
      incomingCommand.remove(0);
    }
    if ( incomingCommand.length() > 11 ){
      Serial.println("Length of command exceeded.");
      incomingCommand.remove(0);
    }

}

// --------------------------------------------------------------------------------------------------


void executeCode(String code) {
  long int value ;
  int spaceIndex = code.indexOf(' '); // Figure out where the space is in the command, else -1
  String command;
  
  if (spaceIndex == -1){
    value = -1;
    if(code.endsWith(".")){ 
      command = code.substring(0,code.indexOf('.'));
    }
    else{
      command = code.substring(0,code.indexOf("?"));
    }
  } 
  else {
    value = code.substring((spaceIndex+1), code.length()).toInt();
    command = code.substring(0,spaceIndex);
  }
    
    // if(command == "NAME OF COMMAND") {
    // Do stuff ; Do More stuff ;
    // }
      
    if(command == "POS"){          // prints position
      Serial.print("Steps from initial: ") ; Serial.println(stepInput);
    }
    if(command == "DIR"){          // prints position
      Serial.print("Motor direction is: ") ; Serial.println(motorDirection);
    }
    if(command == "ZERO"){
      stepInput = 0 ;
      Serial.println("Steps is set to Zero.");
    }
    
    
    return ;
}
