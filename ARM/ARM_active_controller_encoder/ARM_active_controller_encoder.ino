
//Timer interrupt variables
HardwareTimer timer(1);
const long timerMicroseconds = 50 ;  // In microseconds, 50 us = 20KHz period.
                                     // To set your own solve for this:  1,000,000/X = Y Hz
                                     // 1000000 / 50 = 20000 (20KHz)       1000000 / 100 = 10000 (10KHz)

// Closed-loop variables
const int stepperMultiplier = 16 ; // Microstepping multiplier
const int stepperRevolution = (200 * stepperMultiplier) ; // How many full steps to go 1 full revolution
const int encoderRevolution = 800 ; // Steps reported by quadrature encoder for 1 revolution
const int stepEncConvert = (stepperRevolution / encoderRevolution) ; // This gives the factor to multiply the encoder to get equalivalent microsteps

// Quadrature Encoder PIN definitions
const byte encoder0pinA = 15;
const byte encoder0pinB = 16;

// Quadrature variables
volatile long encoder0pos = 0;
volatile bool encoder0pinAlast ;
volatile bool encoder0pinBlast ;

// PIN DEFINITIONS for pololu/A4988 passthrough
const byte stepIN = 25 ;        // The *IN pins come from the RAMPS. We're shimming them so we can do 'stuff' with them
const byte dirIN = 26 ;
const byte motorEnableIN = 27 ; // This var is inverse of what you expect. 0 is ON, 1 is OFF. Read A4988 chip spec
const byte stepOUT = 28;        // The *OUT pins go TO the Pololu stepper driver. 
const byte dirOUT = 29;
const byte motorEnableOUT = 30;

// Stepper variables
volatile long stepInput = 0;     // This is the position in steps the stepper motor has done.
volatile bool motorDirection = 0 ; // 1 is "forward" steps, and 0 is reverse steps
volatile bool motorON = 0 ;

// Serial variables
char incomingByte;                    // a variable to read incoming serial data into
String incomingCommand; // Varible to store the command
bool graphStep = LOW ;
bool graphDir = LOW ;
bool graphEncoder = LOW ;
bool graphDifferential = LOW ;

// ---------Interrupt Declarations-------------------------------------------------------------------


//                     ---------- MOTOR INTERRUPTS ----------
// External Interrupt 10 (D1 D25), steps coming IN. A step represents only 1 step. These can come in FAST, but Mega+RAMPS goes at 10kHz
// A4988 specifies that steps can come in as fast as 1uS (1MHz). If that was the case, we'd be using a Cortex M3 or 4 for timing
void stepTrigger()
{
  if (motorDirection == 1) { stepInput++ ; }          // if motorDirection is 1, add step to stepInput, else subtract 1 from stepInput
  else { stepInput-- ; }
  digitalWrite(stepOUT, 1); // Rapidly toggle the stepOUT pin reasonably fast - possibly will need PORTD method
  digitalWrite(stepOUT, 0);
}

// External Interrupt 9 (D26), direction of motor
void directionTrigger()
{
  motorDirection = digitalRead(dirIN) ; // We cant be sure of initial state, so we always just set it from a read
  digitalWrite(dirOUT, motorDirection) ;
}

// External Interrupt 8 (D27 D32), direction of motor
void motorTrigger()
{
  motorON = digitalRead(motorEnableIN) ;
  digitalWrite(motorEnableOUT, motorON) ;
}
//                     ---------- MOTOR INTERRUPTS END ----------


//                     ---------- ENCODER INTERRUPTS ----------
// External Interrupt 7 (D4 D15), Encoder 0 A channel changed
// If both Pins A and B are equal, we're going clockwise. If different, we're going counterclockwise.
void encoderAtrigger() {

  if ( (!encoder0pinAlast) == digitalRead(encoder0pinB)) { encoder0pos = encoder0pos + 1 ; } 
  else { encoder0pos = encoder0pos - 1 ; }
  
  encoder0pinAlast = !encoder0pinAlast ;
}

// External Interrupt 6 (D4 D15), Encoder 0 A channel changed
// If both Pins A and B are equal, we're going clockwise. If different, we're going counterclockwise.
void encoderBtrigger() {

  if ( (!encoder0pinBlast) == digitalRead(encoder0pinA)) { encoder0pos = encoder0pos - 1 ; } 
  else { encoder0pos = encoder0pos + 1 ; }
  
  encoder0pinBlast = !encoder0pinBlast ;
}
//                     ---------- ENCODER INTERRUPTS END ----------


//                     ---------- TIMER INTERRUPTS ----------
void closedLoop(void) {
  long realSteps = (encoder0pos * stepEncConvert) ; // Encoder data with microstepping multiplier, normalized
  long totalStepOffset = ((stepInput - realSteps) / stepEncConvert) ; // This assures we only move if more than the minimum accuracy of encoder
                                                                      // This outputs a long equal to number of steps divided by the conversion ratio rounded down
  
  if( totalStepOffset > 0 ) {   // Actual steps are lesser than ideal. We need more steps
    digitalWrite(stepOUT, 1) ; 
    digitalWrite(stepOUT, 0) ;
    // stepInput++ ; // We don't want to increment BECAUSE this is a corrective action
  }

  // if ( stepInput == realSteps) {} -- We don't care if they're equal. Everything's happy!

  if( totalStepOffset < 0 ) {   // Actual steps are more than ideal. We need to backtrack steps
    digitalWrite(dirOUT, !motorDirection) ; // Set up opposite direction of motor
    digitalWrite(stepOUT, 1) ;
    digitalWrite(stepOUT, 0) ;
    digitalWrite(dirOUT, motorDirection) ; // restore initial direction of motor
    // stepInput-- ; // We don't want to increment BECAUSE this is a corrective action
  }
  
  return ;
}
//                     ---------- TIMER INTERRUPTS END ----------


// --------------------------------------------------------------------------------------------------


void setup() {
  
  // Timer initialization
  timer.pause(); // Pause to configure the timer interrupt
  timer.setPeriod(timerMicroseconds); // From above, in us
  timer.setChannel1Mode(TIMER_OUTPUT_COMPARE); // Fires interrupt when counter is hit or exceeded
  timer.setCompare(TIMER_CH1, 1);  // Interrupt 1 count after each update
  timer.attachCompare1Interrupt(closedLoop);   // The function to call
  timer.refresh();                 // Refresh the timer's count, prescale, and overflow
  timer.resume();                  // Start the timer
  
  // Pololu pass-through pin assignments
  pinMode(stepIN, INPUT);
  pinMode(dirIN, INPUT);
  pinMode(motorEnableIN, INPUT);
  pinMode(stepOUT, OUTPUT);
  pinMode(dirOUT, OUTPUT);
  pinMode(motorEnableOUT, OUTPUT);

  // Quadrature pin assignments
  pinMode(encoder0pinA, INPUT);
  pinMode(encoder0pinB, INPUT);

  // Interrogate the quadrature pin A state and immediate save it as last
  encoder0pinAlast = digitalRead(encoder0pinA) ;
  
  // Set the Pololu pins to 'not step', whatever the direction is, and whatever the motor status is
  digitalWrite(stepOUT, LOW);
  digitalWrite(dirOUT, digitalRead(dirIN) );
  digitalWrite(motorEnableOUT, digitalRead(motorEnableIN) );

  // Set up interrupts
  attachInterrupt(stepIN, stepTrigger, RISING);
  attachInterrupt(dirIN, directionTrigger, CHANGE);
  attachInterrupt(motorEnableIN, motorTrigger, CHANGE);
  attachInterrupt(encoder0pinA, encoderAtrigger, CHANGE);
  attachInterrupt(encoder0pinB, encoderBtrigger, CHANGE);

  // Enable serial port at listed baudrate
  Serial.begin(115200);
}

void loop() {
  if (graphStep == HIGH) { Serial.println(stepInput); } // SERIOUSLY AFFECTS PERFORMANCE !!!!!!!!! 
  if (graphEncoder == HIGH) { Serial.println(encoder0pos) ; } // SERIOUSLY AFFECTS PERFORMANCE !!!!!!!!!!
  if (graphDir == HIGH){ Serial.println(motorDirection); } // SERIOUSLY AFFECTS PERFORMANCE !!!!!!!!!!
  if (graphDifferential == HIGH){ Serial.print(stepInput); Serial.print("  "); 
                                  Serial.print(encoder0pos*stepEncConvert); Serial.print("  ");  
                                  Serial.println( stepInput-(encoder0pos*stepEncConvert) ); } // SERIOUSLY AFFECTS PERFORMANCE !!!!!!!!!!

    if (Serial.available() > 0) {
      incomingByte = Serial.read();
      incomingCommand.concat(String(incomingByte));
    }
    if ( incomingCommand.endsWith(".") || incomingCommand.endsWith("?") ){
      executeCode(incomingCommand);
      incomingCommand.remove(0);
    }
    if ( incomingCommand.length() > 15 ){
      Serial.println("Length of command exceeded.");
      incomingCommand.remove(0);
    }

}

// --------------------------------------------------------------------------------------------------



void executeCode(String code) {
  long int value ;
  int spaceIndex = code.indexOf(' '); // Figure out where the space is in the command, else -1
  String command;
  
  if (spaceIndex == -1){
    value = -1;
    if(code.endsWith(".")){ 
      command = code.substring(0,code.indexOf('.'));
    }
    else{
      command = code.substring(0,code.indexOf("?"));
    }
  } 
  else {
    value = code.substring((spaceIndex+1), code.length()).toInt();
    command = code.substring(0,spaceIndex);
  }
    
    // if(command == "NAME OF COMMAND") {
    // Do stuff ; Do More stuff ;
    // }
      
    if(command == "POS"){          // prints position
      Serial.print("Steps from initial: ") ; Serial.println(stepInput);
    }
    if(command == "DIR"){          // prints position
      Serial.print("Motor direction is: ") ; Serial.println(motorDirection);
    }
    if(command == "ENCODER"){          // prints current encoder location
      Serial.print("Encoder position is: ") ; Serial.println(encoder0pos);
    }
    if(command == "ZEROSTEP"){
      stepInput = 0 ;
      Serial.println("Steps is set to Zero.");
    }
    if(command == "ZEROENCODER"){
      encoder0pos = 0 ;
      Serial.println("Encoder is set to Zero.");
    }
    if(command == "ENABLE"){
      Serial.print("Motor enable status: ") ; Serial.println(!motorON);
    }
    if(command == "GRAPHSTEP"){
      Serial.println("Graphing Relative step Data: ") ;
      graphStep = HIGH ;
      graphDir = LOW ;
      graphEncoder = LOW ;
      graphDifferential = LOW ;
    }
    if(command == "GRAPHDIR"){
      Serial.println("Graphing Direction Data: ") ; 
      graphStep = LOW ;
      graphDir = HIGH ;
      graphEncoder = LOW ;
      graphDifferential = LOW ;
    }
    if(command == "GRAPHENCODER"){
      Serial.println("Graphing Encoder Data: ") ;
      graphStep = LOW ;
      graphDir = LOW ;
      graphEncoder = HIGH ;
      graphDifferential = LOW ; 
    }
    if(command == "GRAPHCLOSEDLOOP"){
      Serial.println("Graphing Offset Data: ") ;
      graphStep = LOW ;
      graphDir = LOW ;
      graphEncoder = LOW ;
      graphDifferential = HIGH ; 
    }
    if(command == "GRAPHNONE"){
      Serial.println("Graphing turned off: ") ;
      graphStep = LOW ;
      graphDir = LOW ;
      graphEncoder = LOW ;
      graphDifferential = LOW ;
    }
    if(command == "HELP"){
      Serial.println("HELP - Lists this command");
      Serial.println("ENABLE - Reports motor enable status");
      Serial.println("ZEROENCODER - Zeroes Encoder counter");
      Serial.println("ZEROSTEP - Zeroes stepper step counter");
      Serial.println("ENCODER - Reports Encoder location since startup");
      Serial.println("DIR - Reports current motor direction");
      Serial.println("POS - Reports Stepper position since startup");
      Serial.println("GRAPHSTEP - Continously outputs live step data");
      Serial.println("GRAPHDIR - Continously outputs live direction data");
      Serial.println("GRAPHENCODER - Continously outputs live encoder data");
      Serial.println("GRAPHCLOSEDLOOP - Continously outputs live encoder data");
      Serial.println("GRAPHNONE - Turns off graphing");
      
    }
    
    return ;
}



