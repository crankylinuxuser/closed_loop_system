This is a testing suite to verify that the ADNS3080 sensor works correctly on your hardware.

I found existing examples of Atmel chips with ADNS sensors, but none with ARM that worked. This fills the niche of ADNS3080+ARM

[Firmware/Hardware side]
1. Install Arduino 1.8.2
2. Download and install STM32duino
3. Download and load optoflow_arduino to your Maple Mini or similar STM32 hardware.
4. Hook up the ADNS3080
5. Install firmware to Maple Mini.

[UI side]
6. Install Processing (I used version 2.2.1) 
7. Load the ADNS3080_processing sketch.
8. Change your serial port, baudrate, sensor value (if using something else other than ADNS3080), and your multiplier to your needs
9. Press the "Play" button - executes script. 